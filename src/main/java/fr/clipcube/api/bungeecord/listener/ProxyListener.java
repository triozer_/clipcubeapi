package fr.clipcube.api.bungeecord.listener;

import fr.clipcube.api.bungeecord.util.ClipProxyUtils;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.event.ServerKickEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.Random;

/**
 * @author Triozer
 */
public class ProxyListener implements Listener {

    @EventHandler
    public void onJoin(ServerConnectEvent event) {

    }

    @EventHandler
    public void onKick(ServerKickEvent event) {
        int random = new Random().nextInt(ClipProxyUtils.getLobbies().size());

        event.getPlayer().connect(ClipProxyUtils.getLobbies().get(random));

        String reason = BaseComponent.toLegacyText(event.getKickReasonComponent());

        event.getPlayer().sendMessage(new TextComponent("YOU HAVE BEEN KICKED FROM: " + event.getKickedFrom().getName() + " FOR: " + reason));
    }

}
