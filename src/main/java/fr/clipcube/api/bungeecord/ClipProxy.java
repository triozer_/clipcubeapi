package fr.clipcube.api.bungeecord;

import fr.clipcube.api.bungeecord.command.HubCommand;
import fr.clipcube.api.bungeecord.listener.ProxyListener;
import fr.clipcube.api.database.ClipDatabase;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;

/**
 * @author Triozer
 */
public class ClipProxy extends Plugin {
    private static ClipProxy    instance;
    private static ClipDatabase clipDatabase;

    public static ClipProxy getInstance() {
        return instance;
    }

    public static ClipDatabase getClipDatabase() {
        return clipDatabase;
    }

    @Override
    public void onEnable() {
        instance = this;

        System.out.println("MULTI SERVER ENABLING");

        clipDatabase = new ClipDatabase("127.0.0.1", 3307, "clip", "root", "");
        // clipDatabase = new ClipDatabase("127.0.0.1", 3306, "clipcube", "root", "triozer97680");

        registerCommands();
        registerListeners();
    }

    private void registerCommands() {
        new HubCommand().register(this);
    }

    private void registerListeners() {
        ProxyServer.getInstance().getPluginManager().registerListener(this, new ProxyListener());
    }
}
