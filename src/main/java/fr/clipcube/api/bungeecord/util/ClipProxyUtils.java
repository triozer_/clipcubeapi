package fr.clipcube.api.bungeecord.util;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.net.InetSocketAddress;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Triozer
 */
public class ClipProxyUtils {

    public static void check(String type) {
        ServerInfo serverInfo;

        if (!ProxyServer.getInstance().getServers().containsKey("type")) {
            addServer(type, new InetSocketAddress(10000), type + " SERVEUR l CLIPCUBE ", false);
        }

        serverInfo = ProxyServer.getInstance().getServers().get("type");
    }

    public static void addServer(String name, InetSocketAddress address, String motd, boolean restricted) {
        ProxyServer.getInstance().getServers().put(name, ProxyServer.getInstance().constructServerInfo(name, address, motd, restricted));
    }

    public static void removeServer(String name) {
        for (ProxiedPlayer p : ProxyServer.getInstance().getServerInfo(name).getPlayers()) {
            p.disconnect(new TextComponent("This server was forcefully closed."));
        }

        ProxyServer.getInstance().getServers().remove(name);
    }

    public static List<ServerInfo> getLobbies() {
        List<ServerInfo> list = ProxyServer.getInstance().getServers().entrySet().stream()
                .filter(serverInfo -> serverInfo.getKey().contains("lobby"))
                .map(Map.Entry::getValue).collect(Collectors.toList());

        return list;
    }

}
