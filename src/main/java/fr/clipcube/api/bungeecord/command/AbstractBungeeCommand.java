package fr.clipcube.api.bungeecord.command;

import fr.clipcube.api.bungeecord.ClipProxy;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * TODO: ADD PERMISSION CHECK !
 *
 * @author Triozer
 */
public abstract class AbstractBungeeCommand extends Command {
    private String permission;
    private int    minArgs;

    public AbstractBungeeCommand(String name, String permission, int minArgs) {
        super(name);

        this.permission = permission;
        this.minArgs = minArgs;
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        if (minArgs >= 0 && args.length < minArgs) {
            displayHelp((ProxiedPlayer) commandSender);
            return;
        }

        execute((ProxiedPlayer) commandSender, args);
    }

    public void register(ClipProxy clipProxy) {
        clipProxy.getProxy().getPluginManager().registerCommand(clipProxy, this);
    }

    protected abstract void execute(ProxiedPlayer proxiedPlayer, String[] args);

    public abstract void displayHelp(ProxiedPlayer proxiedPlayer);
}