package fr.clipcube.api.bungeecord.command;

import fr.clipcube.api.bungeecord.util.ClipProxyUtils;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.Random;

/**
 * @author Triozer
 */
public class HubCommand extends AbstractBungeeCommand {
    public HubCommand() {
        super("hub", "", 0);
    }

    @Override
    protected void execute(ProxiedPlayer proxiedPlayer, String[] args) {
        if (!proxiedPlayer.getServer().getInfo().getName().contains("lobby")) {
            int random = new Random().nextInt(ClipProxyUtils.getLobbies().size());

            proxiedPlayer.connect(ClipProxyUtils.getLobbies().get(random));
        } else {
            proxiedPlayer.sendMessage(new TextComponent("§cAlready connected to a hub."));
        }
    }

    @Override
    public void displayHelp(ProxiedPlayer proxiedPlayer) {

    }
}
