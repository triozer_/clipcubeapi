package fr.clipcube.api.database;

import fr.clipcube.api.spigot.i18n.ClipLanguage;
import fr.clipcube.api.spigot.player.ClipPlayer;
import fr.clipcube.api.spigot.player.rank.Rank;

import java.sql.*;
import java.util.UUID;

/**
 * @author Triozer
 */
public class ClipDatabase {

    private String host;
    private String username;
    private String password;
    private String database;

    private int port;

    private Connection connection;

    public ClipDatabase(String host, int port, String database, String username, String password) {
        this.host = host;
        this.username = username;
        this.password = password;
        this.database = database;

        this.port = port;
    }

    public void connect() {
        try {
            connection = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database, username, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void disconnect() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private boolean hasAccount(ClipPlayer clipPlayer) {
        connect();

        ResultSet resultSet = query("SELECT * FROM `players` WHERE `uuid`='" + clipPlayer.getUniqueId() + "';");

        try {
            if (resultSet.next()) {
                return true;
            } else
                clipPlayer.firstTime = true;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        disconnect();

        return false;
    }

    public void init(ClipPlayer clipPlayer) {
        if (!hasAccount(clipPlayer)) {
            connect();
            try {
                Statement statement = connection.createStatement();

                execute("INSERT INTO `players` (`id`, `uuid`, `rank`, `coins`) " +
                        "VALUES (NULL, '" + clipPlayer.getUniqueId() + "', 'PLAYER', '1000.0');");

                statement.close();
            } catch (SQLException e) {
                if (e.getMessage().contains("Duplicate entry '" + clipPlayer.getUniqueId() + "' for key 'uuid'"))
                    return;

                e.printStackTrace();
            }

            disconnect();
        }

        cache(clipPlayer);
    }

    private void cache(ClipPlayer clipPlayer) {
        ClipLanguage lang  = getLang(clipPlayer.getUniqueId());
        double       coins = getCoins(clipPlayer.getUniqueId());
        Rank         rank  = getRank(clipPlayer.getUniqueId());

        clipPlayer.getCache().setCoins(coins);
        clipPlayer.getCache().setRank(rank);
        clipPlayer.getCache().setLang(lang);
    }

    public ClipLanguage getLang(UUID uuid) {
        connect();

        String clipLanguage = "FRANCAIS";

        ResultSet resultSet = query("SELECT `lang` FROM `players` WHERE `uuid`='" + uuid + "';");

        try {
            while (resultSet.next()) {
                clipLanguage = resultSet.getString(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        disconnect();

        return ClipLanguage.valueOf(clipLanguage);
    }

    public double getCoins(UUID uuid) {
        connect();

        int coins = 0;

        ResultSet resultSet = query("SELECT `coins` FROM `players` WHERE `uuid`='" + uuid + "';");

        try {
            while (resultSet.next()) {
                coins = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        disconnect();

        return coins;
    }

    public Rank getRank(UUID uuid) {
        connect();

        String rank = "PLAYER";

        ResultSet resultSet = query("SELECT `rank` FROM `players` WHERE `uuid`='" + uuid + "';");

        try {
            while (resultSet.next()) {
                rank = resultSet.getString(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        disconnect();

        return Rank.valueOf(rank);
    }

    public void execute(String query) {
        connect();

        try {
            Statement statement = connection.createStatement();

            statement.executeUpdate(query);

            statement.close();
        } catch (Exception ex) {
            if (!ex.getMessage().contains("Duplicate entry"))
                System.err.println(ex);

            disconnect();
        }

        disconnect();
    }

    public ResultSet query(String query) {
        ResultSet resultSet = null;

        try {
            Statement statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
        } catch (Exception ex) {
            System.err.println(ex);
        }

        return resultSet;
    }

}