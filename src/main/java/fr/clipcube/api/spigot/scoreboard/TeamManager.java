package fr.clipcube.api.spigot.scoreboard;

import fr.clipcube.api.spigot.player.rank.Rank;
import org.bukkit.Bukkit;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

/**
 * @author Triozer
 */
public class TeamManager {

    public static Scoreboard scoreboard;

    public static void init() {
        scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();

        Team player        = scoreboard.registerNewTeam("3");
        Team developer     = scoreboard.registerNewTeam("2");
        Team administrator = scoreboard.registerNewTeam("1");
        Team root          = scoreboard.registerNewTeam("0");

        player.setPrefix(Rank.PLAYER.getTag() + "");
        player.setDisplayName(Rank.PLAYER.getTag() + "");
        player.setOption(Team.Option.NAME_TAG_VISIBILITY, Team.OptionStatus.ALWAYS);
        player.setOption(Team.Option.COLLISION_RULE, Team.OptionStatus.NEVER);

        developer.setPrefix(Rank.DEVELOPER.getPrefix());
        developer.setDisplayName(Rank.DEVELOPER.getPrefix());
        developer.setOption(Team.Option.NAME_TAG_VISIBILITY, Team.OptionStatus.ALWAYS);
        developer.setOption(Team.Option.COLLISION_RULE, Team.OptionStatus.NEVER);

        administrator.setPrefix(Rank.ADMINISTRATOR.getPrefix().substring(0, 7) + " - ");
        administrator.setDisplayName(Rank.ADMINISTRATOR.getPrefix());
        administrator.setOption(Team.Option.NAME_TAG_VISIBILITY, Team.OptionStatus.ALWAYS);
        administrator.setOption(Team.Option.COLLISION_RULE, Team.OptionStatus.NEVER);

        root.setPrefix(Rank.ROOT.getPrefix());
        root.setDisplayName(Rank.ROOT.getPrefix());
        root.setOption(Team.Option.NAME_TAG_VISIBILITY, Team.OptionStatus.ALWAYS);
        root.setOption(Team.Option.COLLISION_RULE, Team.OptionStatus.NEVER);
    }

    public static Scoreboard getScoreboard() {
        return scoreboard;
    }
}
