package fr.clipcube.api.spigot.command;

import fr.clipcube.api.spigot.ClipAPI;
import fr.clipcube.api.spigot.player.ClipPlayer;
import fr.clipcube.api.spigot.player.rank.Rank;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Triozer
 */
public class RankCommand extends AbstractCommand {
    public RankCommand() {
        super("cliprank", "clip.rank", false, 2);
    }

    @Override
    protected void execute(ClipPlayer sender, String[] args) {
        if (args.length == 2) {
            Player target;

            if ((target = Bukkit.getPlayerExact(args[0])) != null) {
                ClipPlayer clipPlayer = ClipAPI.getPlayerManager().getPlayer(target);

                if (StringUtils.isNumeric(args[1])) {
                    Rank rank = Rank.idToRank(Integer.parseInt(args[1]));

                    if (rank != null) {
                        if (clipPlayer.getRank() == rank) {
                            sender.sendVoid();
                            sender.sendTranslatedMessage("command.cliprank.errors.already-rank", clipPlayer.getName(),rank.getName());
                            sender.sendVoid();
                        } else {
                            sender.sendVoid();
                            sender.sendTranslatedMessage("command.cliprank.execute", clipPlayer.getName(), clipPlayer.getRank().getName(), rank.getName());
                            sender.sendVoid();

                            clipPlayer.getCache().setRank(rank);
                            clipPlayer.updateScoreboard();
                        }
                    } else
                        sender.sendTranslatedMessage("command.cliprank.errors.no-rank", args[1]);
                } else if (StringUtils.isAlpha(args[1])) {
                    Rank rank = Rank.nameToRank(args[1]);

                    if (rank != null) {
                        sender.sendVoid();
                        sender.sendTranslatedMessage("command.cliprank.execute", clipPlayer.getName(), clipPlayer.getRank().getName(), rank.getName());
                        sender.sendVoid();

                        clipPlayer.getCache().setRank(rank);
                        clipPlayer.updateScoreboard();
                    } else
                        sender.sendTranslatedMessage("command.cliprank.errors.no-rank", args[1]);
                } else {
                    sender.sendTranslatedMessage("command.cliprank.errors.no-rank", args[1]);
                }

                sender.getCache().save();

            } else {
                sender.sendVoid();
                sender.sendTranslatedMessage("command.no-player", args[0]);
                sender.sendVoid();

            }
        }
    }

    @Override
    public void displayHelp(ClipPlayer sender) {
        sender.sendVoid();
        sender.sendTranslatedMessage("command.cliprank.help");
        sender.sendVoid();
    }

    @Override
    protected List<String> tabCompleter(ClipPlayer sender, String[] args) {
        if (args.length > 1) {

            List<String> list = new ArrayList<>();

            for (Rank rank : Rank.values())
                list.add(rank.getName());

            return list;
        }

        return null;
    }

}