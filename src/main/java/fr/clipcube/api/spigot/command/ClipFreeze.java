package fr.clipcube.api.spigot.command;

import fr.clipcube.api.spigot.ClipAPI;
import fr.clipcube.api.spigot.player.ClipPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * @author Triozer
 */
public class ClipFreeze extends AbstractCommand {
    public ClipFreeze() {
        super("clipfreeze", "clip.freeze", true, 0);
    }

    @Override
    protected void execute(ClipPlayer sender, String[] args) {
        if (args.length == 2) {
            Player     target;
            ClipPlayer clipPlayer;
            int        time;

            if ((target = Bukkit.getPlayerExact(args[0])) == null) {
                sender.sendVoid();
                sender.sendTranslatedMessage("command.no-player", args[0]);
                sender.sendVoid();
                return;
            }

            clipPlayer = ClipAPI.getPlayerManager().getPlayer(target);

            try {
                time = Integer.parseInt(args[1]);
            } catch (NumberFormatException e) {
                sender.sendVoid();
                sender.sendTranslatedMessage("command.clipfreeze.errors.number-format-exception", args[1]);
                sender.sendVoid();
                return;
            }

            clipPlayer.freeze(time);

            sender.sendVoid();
            sender.sendTranslatedMessage("command.clipfreeze.execute", clipPlayer.getName(), args[1]);
            sender.sendVoid();
            clipPlayer.sendVoid();
            clipPlayer.sendTranslatedMessage("command.clipfreeze.execute-target", args[1]);
            clipPlayer.sendVoid();
        } else {
            displayHelp(sender);
        }
    }

    @Override
    public void displayHelp(ClipPlayer sender) {
        sender.sendVoid();
        sender.sendTranslatedMessage("command.clipfreeze.help");
        sender.sendVoid();
    }

    @Override
    protected List<String> tabCompleter(ClipPlayer sender, String[] args) {
        return null;
    }
}
