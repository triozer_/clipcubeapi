package fr.clipcube.api.spigot.command;

import fr.clipcube.api.spigot.player.ClipPlayer;

import java.util.List;

/**
 * @author Triozer
 */
public class PluginCommand extends AbstractCommand {
    public PluginCommand() {
        super("plugins", "", true, 0);
    }

    @Override
    protected void execute(ClipPlayer sender, String[] args) {
        sender.sendVoid();
        sender.sendVoid();
    }

    @Override
    public void displayHelp(ClipPlayer sender) {

    }

    @Override
    protected List<String> tabCompleter(ClipPlayer sender, String[] args) {
        return null;
    }
}
