package fr.clipcube.api.spigot.command;

import fr.clipcube.api.spigot.ClipAPI;
import fr.clipcube.api.spigot.player.ClipPlayer;
import fr.clipcube.api.spigot.util.InventoryBuilder;
import fr.clipcube.api.spigot.util.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;

import java.util.List;

/**
 * @author Triozer
 */
public class CoinsCommand extends AbstractCommand {
    public CoinsCommand() {
        super("clipcoins", null, true, 0);
    }

    @Override
    protected void execute(ClipPlayer sender, String[] args) {
        if (args.length == 0) {
            displayHelp(sender);
        } else if (args.length == 2) {
            if (args[0].equalsIgnoreCase("see")) {
                if (sender.getRank().hasPermission("clip.coins.see") || sender.isOp()) {
                    Player target;

                    if ((target = Bukkit.getPlayerExact(args[1])) != null) {
                        ClipPlayer clipPlayer = ClipAPI.getPlayerManager().getPlayer(target);

                        sender.sendVoid();
                        sender.sendMessage(clipPlayer.getRank().getTag() + clipPlayer.getName() + " §fhas §a"
                                + clipPlayer.getCoins() + " §fcoins.");
                        sender.sendVoid();
                    } else {
                        displayHelp(sender);
                    }
                } else {
                    displayHelp(sender);
                }
            } else if (args[0].equalsIgnoreCase("edit")) {
                if (sender.getRank().hasPermission("clip.coins.edit") || sender.isOp()) {
                    Player target;

                    if ((target = Bukkit.getPlayerExact(args[1])) != null) {
                        ClipPlayer clipPlayer = ClipAPI.getPlayerManager().getPlayer(target);

                        sender.openInventory(new InventoryBuilder("Edit §b" + clipPlayer.getName() + " §rcoins", InventoryType.HOPPER)
                                .setItem(0, new ItemBuilder(Material.DIAMOND).name("§aADD COINS.").build())
                                .setItem(1, new ItemBuilder(Material.REDSTONE).name("§cTAKE COINS.").build())
                                .setItem(4, new ItemBuilder(Material.BARRIER).name("§eCLOSE GUI.").build())
                                .setListener(new InventoryBuilder.InventoryListener() {
                                    @Override
                                    public void interact(ClipPlayer player, InventoryClickEvent event) {
                                        if (event.getCurrentItem() != null)
                                            switch (event.getCurrentItem().getType()) {
                                                case DIAMOND:
                                                    player.closeInventory();

                                                    player.openInventory(new InventoryBuilder("Add coins §b" + clipPlayer.getName(), 9)
                                                            .setItem(0, new ItemBuilder(Material.STAINED_GLASS_PANE)
                                                                    .name("§7Add one coin.").data((byte) 7).build())
                                                            .setItem(1, new ItemBuilder(Material.STAINED_GLASS_PANE)
                                                                    .name("§eAdd five coins.").data((byte) 4).build())
                                                            .setItem(2, new ItemBuilder(Material.STAINED_GLASS_PANE)
                                                                    .name("§6Add ten coins.").data((byte) 1).build())
                                                            .setItem(3, new ItemBuilder(Material.STAINED_GLASS_PANE)
                                                                    .name("§cAdd fifty coins.").data((byte) 6).build())
                                                            .setItem(4, new ItemBuilder(Material.STAINED_GLASS_PANE)
                                                                    .name("§4Add hundred coins.").data((byte) 14).build())
                                                            .setItem(8, new ItemBuilder(Material.BARRIER)
                                                                    .name("§eCLOSE GUI.").build())
                                                            .build(ClipAPI.getInstance()));
                                                    break;
                                                case REDSTONE:
                                                    player.closeInventory();

                                                    player.openInventory(new InventoryBuilder("Take coins §b" + clipPlayer.getName(), 9)
                                                            .setItem(0, new ItemBuilder(Material.STAINED_GLASS_PANE)
                                                                    .name("§7Take one coin.").data((byte) 7).build())
                                                            .setItem(1, new ItemBuilder(Material.STAINED_GLASS_PANE)
                                                                    .name("§eTake five coins.").data((byte) 4).build())
                                                            .setItem(2, new ItemBuilder(Material.STAINED_GLASS_PANE)
                                                                    .name("§6Take ten coins.").data((byte) 1).build())
                                                            .setItem(3, new ItemBuilder(Material.STAINED_GLASS_PANE)
                                                                    .name("§cTake fifty coins.").data((byte) 6).build())
                                                            .setItem(4, new ItemBuilder(Material.STAINED_GLASS_PANE)
                                                                    .name("§4Take hundred coins.").data((byte) 14).build())
                                                            .setItem(8, new ItemBuilder(Material.BARRIER)
                                                                    .name("§eCLOSE GUI.").build())
                                                            .build(ClipAPI.getInstance()));
                                                    break;
                                                case BARRIER:
                                                    player.closeInventory();
                                                    break;
                                                default:
                                                    break;
                                            }

                                        event.setCancelled(true);
                                    }
                                }).build(ClipAPI.getInstance()));
                    } else {
                        sender.sendMessage("§cHumm, cant find this player.");
                    }
                } else {
                    displayHelp(sender);
                }
            } else
                displayHelp(sender);
        } else if (args.length == 3) {
            if (args[0].equalsIgnoreCase("set")) {
                if (sender.getRank().hasPermission("clip.coins.set") || sender.isOp()) {
                    Player target;

                    if ((target = Bukkit.getPlayerExact(args[1])) != null) {
                        ClipPlayer clipPlayer = ClipAPI.getPlayerManager().getPlayer(target);
                        double     coins      = 0;

                        try {
                            coins = Double.valueOf(args[2]);
                        } catch (NumberFormatException e) {
                            sender.sendMessage("§cThe argument must be a number.");
                            return;
                        }

                        if (coins >= 0) {
                            clipPlayer.getCache().setCoins(coins);
                            clipPlayer.updateScoreboard();

                            sender.sendMessage("§b" + target.getName() + " §ahas now §6" + clipPlayer.getCoins() + " §acoins.");
                        } else
                            sender.sendMessage("§cThe number must be higher than 0. §7(" + coins + " < 0)");
                    } else {
                        sender.sendMessage("§cHumm, cant find this player.");
                    }
                } else {
                    displayHelp(sender);
                }
            }
        } else {
            displayHelp(sender);
        }
    }

    @Override
    public void displayHelp(ClipPlayer sender) {
        sender.sendVoid();
        sender.sendMessage("§fYou have §a" + sender.getCoins() + " §fcoins.");
        sender.sendVoid();
    }

    @Override
    protected List<String> tabCompleter(ClipPlayer sender, String[] args) {
        return null;
    }
}
