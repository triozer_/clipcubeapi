package fr.clipcube.api.spigot.command;

import fr.clipcube.api.spigot.ClipAPI;
import fr.clipcube.api.spigot.player.ClipPlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;

/**
 * @author Triozer
 */
public abstract class AbstractCommand implements CommandExecutor, TabCompleter {

    private String  commandName;
    private String  permission;
    private boolean onlyPlayersCommand;
    private int     minArgs;

    public AbstractCommand(String commandName, String permission, boolean onlyPlayersCommand, int minArgs) {
        this.commandName = commandName;
        this.permission = permission;
        this.onlyPlayersCommand = onlyPlayersCommand;
        this.minArgs = minArgs;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (onlyPlayersCommand && !(sender instanceof Player)) {
            sender.sendMessage("§cOnly a player can execute this command.");
            return true;
        }

        ClipPlayer senderPlayer = ClipAPI.getPlayerManager().getPlayer((Player) sender);

        if (permission != null)
            if (!senderPlayer.hasPermission(permission) && !(senderPlayer.getRank().getId() >= 2)) {
                senderPlayer.sendTranslatedMessage("command.no-permission");
                return true;
            }

        if (minArgs >= 0 && args.length < minArgs) {
            displayHelp(senderPlayer);
            return true;
        }

        execute(senderPlayer, args);

        return false;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        if (label.equalsIgnoreCase(commandName)) {
            ClipPlayer senderPlayer = ClipAPI.getPlayerManager().getPlayer((Player) sender);

            if (permission != null)
                if (!senderPlayer.hasPermission(permission) && !(senderPlayer.getRank().getId() >= 2)) {
                    return null;
                }

            return tabCompleter(senderPlayer, args);
        }

        return null;
    }

    public void register(JavaPlugin plugin) {
        plugin.getCommand(commandName).setExecutor(this);
        plugin.getCommand(commandName).setTabCompleter(this);
    }

    protected abstract void execute(ClipPlayer sender, String[] args);

    public abstract void displayHelp(ClipPlayer sender);

    protected abstract List<String> tabCompleter(ClipPlayer sender, String[] args);
}
