package fr.clipcube.api.spigot.command;

import fr.clipcube.api.spigot.i18n.ClipLanguage;
import fr.clipcube.api.spigot.player.ClipPlayer;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Triozer
 */
public class LanguageCommand extends AbstractCommand {
    public LanguageCommand() {
        super("cliplanguage", null, true, 0);
    }

    @Override
    protected void execute(ClipPlayer sender, String[] args) {
        if (args.length == 0) {
            displayHelp(sender);
        } else if (args.length == 1) {
            ClipLanguage old = sender.getLanguage();
            ClipLanguage clipLanguage;

            if ((clipLanguage = ClipLanguage.langToEnum(args[0])) == null) {
                sender.sendVoid();
                sender.sendTranslatedMessage("command.cliplanguage.errors.void", args[0]);
                sender.sendVoid();
                return;
            }

            sender.getCache().setLang(clipLanguage);

            sender.sendVoid();
            sender.sendTranslatedMessage("command.cliplanguage.execute", old.getLang(), clipLanguage.getLang());
            sender.sendVoid();

            sender.updateScoreboard();
        }
    }

    @Override
    public void displayHelp(ClipPlayer sender) {
        sender.sendVoid();
        sender.sendTranslatedMessage("command.cliplanguage.help");
        sender.sendVoid();
    }

    @Override
    protected List<String> tabCompleter(ClipPlayer sender, String[] args) {
        List<String> languages = new ArrayList<>();

        for (ClipLanguage language : ClipLanguage.values())
            languages.add(language.getLang());

        return languages;
    }
}
