package fr.clipcube.api.spigot.command;

import fr.clipcube.api.spigot.ClipAPI;
import fr.clipcube.api.spigot.i18n.SimplyTranslation;
import fr.clipcube.api.spigot.player.ClipPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * @author Triozer
 */
public class InfoCommand extends AbstractCommand {
    public InfoCommand() {
        super("clipinfo", null, true, 0);
    }

    @Override
    protected void execute(ClipPlayer sender, String[] args) {
        if (args.length == 0) {
            displayHelp(sender);
        } else if (args.length == 1) {
            if (sender.getRank().hasPermission("clip.info.see") || sender.isOp()) {
                Player target;

                if ((target = Bukkit.getPlayerExact(args[0])) != null) {
                    ClipPlayer clipPlayer = ClipAPI.getPlayerManager().getPlayer(target);

                    sender.sendVoid();
                    sender.sendMessage(SimplyTranslation.translateClipName(sender, clipPlayer) + " §7(" + clipPlayer.getHandle().ping + " ms) "
                            + "§fconnected to §b" + clipPlayer.getClipServer().getId()
                            + " §7(" + clipPlayer.getClipServer().getPlayers().size() + "/"
                            + clipPlayer.getClipServer().getCraftServer().getMaxPlayers() + ")");
                    sender.sendVoid();
                } else {
                    displayHelp(sender);
                }
            } else {
                displayHelp(sender);
            }
        } else {
            displayHelp(sender);
        }
    }

    @Override
    public void displayHelp(ClipPlayer sender) {
        sender.sendVoid();
        sender.sendMessage(SimplyTranslation.translateClipName(sender, sender) + " §7(" + sender.getHandle().ping + " ms) "
                + "§fconnected to §b" + sender.getClipServer().getId()
                + " §7(" + sender.getClipServer().getPlayers().size() + "/50)");
        sender.sendVoid();
    }

    @Override
    protected List<String> tabCompleter(ClipPlayer sender, String[] args) {
        return null;
    }
}
