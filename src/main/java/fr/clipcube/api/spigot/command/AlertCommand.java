package fr.clipcube.api.spigot.command;

import fr.clipcube.api.spigot.ClipAPI;
import fr.clipcube.api.spigot.player.ClipPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.List;

/**
 * @author Triozer
 */
public class AlertCommand extends AbstractCommand {
    public AlertCommand() {
        super("clipalert", "clip.alert", true, 0);
    }

    @Override
    protected void execute(ClipPlayer sender, String[] args) {
        if (args.length > 1) {
            Player     target;
            ClipPlayer clipPlayer = null;

            if ((target = Bukkit.getPlayerExact(args[0])) != null) {
                clipPlayer = ClipAPI.getPlayerManager().getPlayer(target);
            }

            String string = args[2];

            for (int i = 3; i < args.length; i++)
                string += " " + args[i];

            if (args[1].equalsIgnoreCase("ab")) {
                clipPlayer.sendActionBar(string);
            } else if (args[1].equalsIgnoreCase("item")) {
                clipPlayer.sendHologram(30, new ItemStack(Material.DIAMOND));
            } else if (args[1].equalsIgnoreCase("holo")) {
                clipPlayer.sendHologram(30, string.split(" "));
            } else if (args[1].equalsIgnoreCase("center")) {
                clipPlayer.sendCenteredMessage(string);
            }

        }
    }

    @Override
    public void displayHelp(ClipPlayer sender) {

    }

    @Override
    protected List<String> tabCompleter(ClipPlayer sender, String[] args) {
        return null;
    }
}
