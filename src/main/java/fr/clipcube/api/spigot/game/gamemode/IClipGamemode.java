package fr.clipcube.api.spigot.game.gamemode;

/**
 * @author Triozer
 */
public interface IClipGamemode {
    String getGameDescription();

    String getGameName();

    String getGamePrefix();
}
