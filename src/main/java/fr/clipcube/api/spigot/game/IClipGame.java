package fr.clipcube.api.spigot.game;

/**
 * @author Triozer
 */
public interface IClipGame {
    String getGameDescription();

    String getGameName();

    String getGamePrefix();

    String getAuthor();
}
