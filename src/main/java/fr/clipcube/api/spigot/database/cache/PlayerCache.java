package fr.clipcube.api.spigot.database.cache;

import fr.clipcube.api.spigot.ClipAPI;
import fr.clipcube.api.spigot.i18n.ClipLanguage;
import fr.clipcube.api.spigot.player.ClipPlayer;
import fr.clipcube.api.spigot.player.rank.Rank;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Triozer
 */
public class PlayerCache {

    private Map<String, Object> cache = new HashMap<>();
    private ClipPlayer clipPlayer;

    public PlayerCache(ClipPlayer clipPlayer) {
        this.clipPlayer = clipPlayer;

        cache.put("coins", 1000.0);
        cache.put("rank", Rank.PLAYER);
        cache.put("lang", ClipLanguage.ENGLISH);
    }

    public void save() {
        ClipAPI.getClipDatabase().
                execute("UPDATE players SET coins='" + getCoins() + "', rank='" + getRank() +
                        "', lang='" + getLang() + "' WHERE uuid='" + clipPlayer.getUniqueId() + "';");
    }

    public double getCoins() {
        return (double) cache.get("coins");
    }

    public void setCoins(double coins) {
        cache.replace("coins", coins);
    }

    public void addCoins(double coins) {
        cache.replace("coins", getCoins() + coins);
    }

    public void removeCoins(double coins) {
        if ((double) cache.get("coins") - coins >= 0)
            cache.replace("coins", getCoins() - coins);
    }

    public Rank getRank() {
        return (Rank) cache.get("rank");
    }

    public void setRank(Rank rank) {
        cache.replace("rank", rank);
    }

    public ClipLanguage getLang() {
        return (ClipLanguage) cache.get("lang");
    }

    public void setLang(ClipLanguage clipLang) {
        cache.replace("lang", clipLang);
    }
}