package fr.clipcube.api.spigot.ishield;

import fr.clipcube.api.spigot.ClipModule;

/**
 * @author Triozer
 */
public class IShield extends ClipModule {
    public IShield() {
        super("ClipAPI", "IShield", "The internal anticheat module.");
    }

    @Override
    public void onLoad() {
        super.onLoad();
    }

    @Override
    public void onEnable() {
        super.onEnable();
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }
}
