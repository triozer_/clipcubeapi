package fr.clipcube.api.spigot.listener;

import fr.clipcube.api.spigot.ClipAPI;
import fr.clipcube.api.spigot.player.ClipPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

/**
 * @author Triozer
 */
public class GuiListener implements Listener {
    @EventHandler
    public void inventory(InventoryClickEvent event) {
        ClipPlayer player = ClipAPI.getPlayerManager().getPlayer((Player) event.getWhoClicked());

        if (event.getCurrentItem() == null || event.getCurrentItem().getType() == Material.AIR)
            return;

        if (event.getClickedInventory().getName().contains("coins")) {
            ClipPlayer target;

            if (event.getClickedInventory().getName().contains("Add coins")) {
                String targetName = event.getClickedInventory().getName().substring(12);
                target = ClipAPI.getPlayerManager().getPlayer(Bukkit.getPlayerExact(targetName));

                switch (event.getCurrentItem().getItemMeta().getDisplayName()) {
                    case "§7Add one coin.":
                        target.getCache().addCoins(1.0);
                        player.sendMessage("§aAdded §71 §acoin to §b" + target.getName() + "§a. §7Now he has " + target.getCoins());
                        break;
                    case "§eAdd five coins.":
                        target.getCache().addCoins(5.0);
                        player.sendMessage("§aAdded §e5 §acoins to §b" + target.getName() + "§a. §7Now he has " + target.getCoins());
                        break;
                    case "§6Add ten coins.":
                        target.getCache().addCoins(10.0);
                        player.sendMessage("§aAdded §610 §acoins to §b" + target.getName() + "§a. §7Now he has " + target.getCoins());
                        break;
                    case "§cAdd fifty coins.":
                        target.getCache().addCoins(50.0);
                        player.sendMessage("§aAdded §c50 §acoins to §b" + target.getName() + "§a. §7Now he has " + target.getCoins());
                        break;
                    case "§4Add hundred coins.":
                        target.getCache().addCoins(100.0);
                        player.sendMessage("§aAdded §4100 §acoins to §b" + target.getName() + "§a. §7Now he has " + target.getCoins());
                        break;
                    case "§eCLOSE GUI.":
                        player.closeInventory();
                        break;
                    default:
                        break;
                }

                player.updateScoreboard();
                event.setCancelled(true);
            } else if (event.getClickedInventory().getName().contains("Take coins")) {
                String targetName = event.getClickedInventory().getName().substring(13);
                target = ClipAPI.getPlayerManager().getPlayer(Bukkit.getPlayerExact(targetName));

                switch (event.getCurrentItem().getItemMeta().getDisplayName()) {
                    case "§7Take one coin.":
                        target.getCache().removeCoins(1.0);
                        player.sendMessage("§aTaken §71 §acoin to §b" + target.getName() + "§a. §7Now he has " + target.getCoins());
                        break;
                    case "§eTake five coins.":
                        target.getCache().removeCoins(5.0);
                        player.sendMessage("§aTaken §e5 §acoins to §b" + target.getName() + "§a. §7Now he has " + target.getCoins());
                        break;
                    case "§6Take ten coins.":
                        target.getCache().removeCoins(10.0);
                        player.sendMessage("§aTaken §610 §acoins to §b" + target.getName() + "§a. §7Now he has " + target.getCoins());
                        break;
                    case "§cTake fifty coins.":
                        target.getCache().removeCoins(50.0);
                        player.sendMessage("§aTaken §c50 §acoins to §b" + target.getName() + "§a. §7Now he has " + target.getCoins());
                        break;
                    case "§4Take hundred coins.":
                        target.getCache().removeCoins(100.0);
                        player.sendMessage("§aTaken §4100 §acoins to §b" + target.getName() + "§a. §7Now he has " + target.getCoins());
                        break;
                    case "§eCLOSE GUI.":
                        player.closeInventory();
                        break;
                    default:
                        break;
                }

                player.updateScoreboard();
                event.setCancelled(true);
            }
        }
    }
}
