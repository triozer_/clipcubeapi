package fr.clipcube.api.spigot.listener;

import fr.clipcube.api.spigot.ClipAPI;
import fr.clipcube.api.spigot.i18n.SimplyTranslation;
import fr.clipcube.api.spigot.player.ClipPlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 * @author Triozer
 */
public class PlayerListener implements Listener {
    @EventHandler
    public void move(PlayerMoveEvent event) {
        ClipPlayer clipPlayer;

        if ((clipPlayer = ClipAPI.getPlayerManager().getPlayer(event.getPlayer())) != null)
            if (clipPlayer.freezed) {
                event.setCancelled(true);
                return;
            }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        ClipPlayer clipPlayer = ClipAPI.getPlayerManager().getPlayer(event.getPlayer());

        if (clipPlayer.freezed) {
            event.setCancelled(true);
            return;
        }
    }

    @EventHandler
    public void onInteractEntity(PlayerInteractAtEntityEvent event) {
        ClipPlayer clipPlayer = ClipAPI.getPlayerManager().getPlayer(event.getPlayer());

        if (clipPlayer.freezed) {
            event.setCancelled(true);
            return;
        }

    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        ClipPlayer clipPlayer = ClipAPI.getPlayerManager().getPlayer(event.getPlayer());

        if (clipPlayer.freezed) {
            clipPlayer.sendTranslatedMessage("chat.freeze");
            event.setCancelled(true);
            return;
        }

        if (!clipPlayer.getRank().hasPermission("clip.chat.spam"))
            if (System.currentTimeMillis() - clipPlayer.lastMessage <= 1500 || clipPlayer.lastString == event.getMessage()) {
                clipPlayer.sendTranslatedMessage("chat.spam");

                clipPlayer.lastMessage = System.currentTimeMillis();
                clipPlayer.lastString = event.getMessage();
                event.setCancelled(true);

                return;
            }

        if (clipPlayer.getRank().getId() > 0)
            event.setFormat("{0}: §r" + event.getMessage());
        else
            event.setFormat("{0}: §7" + event.getMessage());

        event.setCancelled(true);

        for (ClipPlayer clipPlayers : ClipAPI.getPlayerManager().getPlayers())
            clipPlayers.sendMessage(event.getFormat().replace("{0}", SimplyTranslation.translateClipName(clipPlayers, clipPlayer)));

        clipPlayer.lastMessage = System.currentTimeMillis();
        clipPlayer.lastString = event.getMessage();
    }

}
