package fr.clipcube.api.spigot;

/**
 * @author Triozer
 */
public abstract class ClipModule {
    private String   plugin;
    private String   name;
    private String[] description;

    public ClipModule(String plugin, String name, String... description) {
        this.plugin = plugin;
        this.name = name;
        this.description = description;

        ClipAPI.getClipModule().add(this);
    }

    public void onLoad() {
        System.out.println("[" + plugin + "][" + name + "] Loaded.");
    }

    public void onEnable() {
        System.out.println("[" + plugin + "][" + name + "] Enabled.");
    }

    public void onDisable() {
        System.out.println("[" + plugin + "][" + name + "] Disabled.");
    }

    public String description() {
        String message = description[0];

        for (int i = 1; i < description.length; i++)
            message += " " + message;

        return message;
    }

    public String getName() {
        return name;
    }
}