package fr.clipcube.api.spigot.player.rank;

import fr.clipcube.api.spigot.scoreboard.TeamManager;
import org.bukkit.ChatColor;
import org.bukkit.scoreboard.Team;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Triozer
 */
public enum Rank {

    ROOT(3, "Root", ChatColor.AQUA, TeamManager.getScoreboard().getTeam("0")),
    ADMINISTRATOR(2, "Administrator", ChatColor.RED, TeamManager.getScoreboard().getTeam("1")),
    DEVELOPER(1, "Developer", ChatColor.GOLD, TeamManager.getScoreboard().getTeam("2")),
    PLAYER(0, "Player", ChatColor.GRAY, TeamManager.getScoreboard().getTeam("3"));

    int    id;
    String name;

    ChatColor tag;

    Team team;

    List<String> permissionsList = new ArrayList<>();

    Rank(int id, String name, ChatColor tag, Team team) {
        this.id = id;
        this.name = name;
        this.tag = tag;
        this.team = team;
    }

    public static Rank idToRank(int id) {
        switch (id) {
            case 3:
                return ROOT;
            case 2:
                return ADMINISTRATOR;
            case 1:
                return DEVELOPER;
            case 0:
                return PLAYER;
            default:
                return null;
        }
    }

    public static Rank nameToRank(String name) {
        switch (name) {
            case "Root":
                return ROOT;
            case "Administrator":
                return ADMINISTRATOR;
            case "Developer":
                return DEVELOPER;
            case "Player":
                return PLAYER;
            default:
                return null;
        }
    }

    public void addPermission(String... permissions) {
        permissionsList.addAll(Arrays.asList(permissions).stream().collect(Collectors.toList()));
    }

    public void removePermission(String... permissions) {
        permissionsList.removeAll(Arrays.asList(permissions).stream().collect(Collectors.toList()));
    }

    public boolean hasPermission(String permission) {
        if (this == Rank.ROOT || this == Rank.ADMINISTRATOR)
            return true;

        return permissionsList.contains(permission);
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPrefix() {
        return getTag() + getName() + " – ";
    }

    public ChatColor getTag() {
        return tag;
    }

    public Team getTeam() {
        return team;
    }
}
