package fr.clipcube.api.spigot.player;

import fr.clipcube.api.spigot.ClipAPI;
import fr.clipcube.api.spigot.database.cache.PlayerCache;
import fr.clipcube.api.spigot.i18n.ClipLanguage;
import fr.clipcube.api.spigot.i18n.SimplyTranslation;
import fr.clipcube.api.spigot.player.rank.Rank;
import fr.clipcube.api.spigot.proxy.ClipServer;
import fr.clipcube.api.spigot.scoreboard.IndividualScoreboard;
import fr.clipcube.api.spigot.util.DefaultFontInfo;
import fr.clipcube.api.spigot.util.HologramBuilder;
import fr.clipcube.api.spigot.util.packet.EntityHider;
import net.md_5.bungee.api.chat.TextComponent;
import net.minecraft.server.v1_10_R1.IChatBaseComponent;
import net.minecraft.server.v1_10_R1.PacketPlayOutChat;
import net.minecraft.server.v1_10_R1.PacketPlayOutPlayerListHeaderFooter;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_10_R1.CraftServer;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.lang.reflect.Field;
import java.util.List;

/**
 * @author Triozer
 */
public class ClipPlayer extends CraftPlayer {

    public boolean firstTime   = false;
    public boolean freezed     = false;
    public long    lastMessage = 0;
    public String  lastString  = "";
    private IndividualScoreboard individualScoreboard;
    private PlayerCache cache = null;

    public ClipPlayer(Player player) {
        super((CraftServer) player.getServer(), ((CraftPlayer) player).getHandle());

        cache = new PlayerCache(this);
    }

    public void make(IndividualScoreboard individualScoreboard) {
        this.individualScoreboard = individualScoreboard;

        individualScoreboard.make();
    }

    public void updateScoreboard() {
        if (getClipServer() != null && getClipServer().getId().contains("hub-")) {
            individualScoreboard.setLine(1, translate("scoreboard.name", getName()));
            individualScoreboard.setLine(2, translate("scoreboard.rank", SimplyTranslation.translateRank(this, getRank())));
            individualScoreboard.setLine(3, translate("scoreboard.lang", cache.getLang().getLang()));
            individualScoreboard.setLine(5, "Coins: §a" + cache.getCoins());
            individualScoreboard.setLine(7, translate("scoreboard.server-id", "" + getClipServer().getId()));
        }
    }

    public void freeze(int time) {
        freezed = true;

        if (time != -1)
            Bukkit.getScheduler().runTaskLater(ClipAPI.getInstance(), () -> freezed = false, 20 * time);
    }

    public void sendActionBar(String message) {
        IChatBaseComponent a                 = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + message + "\"}");
        PacketPlayOutChat  packetPlayOutChat = new PacketPlayOutChat(a, (byte) 2);

        getHandle().playerConnection.sendPacket(packetPlayOutChat);
    }

    public void sendTablist(String header, String footer) {
        IChatBaseComponent                  a      = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + header + "\"}");
        IChatBaseComponent                  b      = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + footer + "\"}");
        PacketPlayOutPlayerListHeaderFooter packet = new PacketPlayOutPlayerListHeaderFooter();

        try {
            if (header != null) {
                Field hfield = packet.getClass().getDeclaredField("a");
                hfield.setAccessible(true);
                hfield.set(packet, a);
                hfield.setAccessible(false);
            }
            if (footer != null) {
                Field ffield = packet.getClass().getDeclaredField("b");
                ffield.setAccessible(true);
                ffield.set(packet, b);
                ffield.setAccessible(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        getHandle().playerConnection.sendPacket(packet);
    }

    public void sendHologram(int time, String... lines) {
        HologramBuilder hologramBuilder = new HologramBuilder(getLocation()).lines(lines).time(time).build();

        EntityHider entityHider = new EntityHider(ClipAPI.getInstance(), EntityHider.Policy.BLACKLIST);

        for (ClipPlayer clipPlayer : ClipAPI.getPlayerManager().getPlayers())
            for (ArmorStand armorStand : hologramBuilder.getArmorStands())
                if (clipPlayer != this)
                    entityHider.hideEntity(clipPlayer.getPlayer(), armorStand);
    }

    public void sendHologram(int time, ItemStack itemStack) {
        HologramBuilder hologramBuilder = new HologramBuilder(getLocation()).item(itemStack).time(time).build();

        EntityHider entityHider = new EntityHider(ClipAPI.getInstance(), EntityHider.Policy.BLACKLIST);


        for (ClipPlayer clipPlayer : ClipAPI.getPlayerManager().getPlayers())
            for (ArmorStand armorStand : hologramBuilder.getArmorStands())
                if (clipPlayer != this) {
                    entityHider.hideEntity(clipPlayer.getPlayer(), hologramBuilder.getItem());
                    entityHider.hideEntity(clipPlayer.getPlayer(), armorStand);
                }
    }

    public void sendVoid() {
        this.sendMessage("");
    }

    public void sendCenteredMessage(String message) {
        if (message == null || message.equals(""))
            return;

        int     messagePxSize = 0;
        boolean previousCode  = false;
        boolean isBold        = false;

        for (char c : message.toCharArray()) {
            if (c == '§') {
                previousCode = true;
                continue;
            } else if (previousCode == true) {
                previousCode = false;
                if (c == 'l' || c == 'L') {
                    isBold = true;
                    continue;
                } else isBold = false;
            } else {
                DefaultFontInfo dFI = DefaultFontInfo.getDefaultFontInfo(c);
                messagePxSize += isBold ? dFI.getBoldLength() : dFI.getLength();
                messagePxSize++;
            }
        }

        int halvedMessageSize = messagePxSize / 2;
        int toCompensate      = DefaultFontInfo.CENTER_PX - halvedMessageSize;
        int spaceLength       = DefaultFontInfo.SPACE.getLength() + 1;
        int compensated       = 0;

        StringBuilder sb = new StringBuilder();

        while (compensated < toCompensate) {
            sb.append(" ");
            compensated += spaceLength;
        }

        this.sendMessage(sb.toString() + message);
    }

    public void sendTranslatedMessage(String path, String... replace) {
        this.sendMessage(translate(path, replace));
    }

    public void sendMessage(List<String> strings) {
        strings.forEach(s -> sendMessage(s));
    }

    public void sendTextComponent(TextComponent... textComponent) {
        TextComponent text = new TextComponent(textComponent[0]);

        for (int i = 1; i < textComponent.length; i++)
            text.addExtra(textComponent[i]);

        this.spigot().sendMessage(text);
    }

    public void sendTo(String serverName) {
        try {
            ByteArrayOutputStream b   = new ByteArrayOutputStream();
            DataOutputStream      out = new DataOutputStream(b);

            out.writeUTF("Connect");
            out.writeUTF(serverName);

            Bukkit.getPlayer(this.getUniqueId()).sendPluginMessage(ClipAPI.getInstance(), "BungeeCord", b.toByteArray());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public String translate(String path, String... replace) {
        return ClipLanguage.translate(getLanguage(), path, replace);
    }

    public double getCoins() {
        return cache.getCoins();
    }

    public PlayerCache getCache() {
        return cache;
    }

    public Rank getRank() {
        return cache.getRank();
    }

    public String getClipName() {
        return getRank().getTag() + "[" + getRank().getName() + "] " + getName();
    }

    public ClipServer getClipServer() {
        return ClipAPI.getInstance().getClipServer();
    }

    public IndividualScoreboard getIndividualScoreboard() {
        return individualScoreboard;
    }

    public ClipLanguage getLanguage() {
        return cache.getLang();
    }

    public boolean isFreezed() {
        return freezed;
    }

}

