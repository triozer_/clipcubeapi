package fr.clipcube.api.spigot.player;

import fr.clipcube.api.spigot.ClipAPI;
import fr.clipcube.api.spigot.scoreboard.TeamManager;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.*;

public class PlayerManager {

    private List<ClipPlayer>      players;
    private Map<UUID, ClipPlayer> playerToClipPlayer;

    public PlayerManager() {
        players = new ArrayList<>();
        playerToClipPlayer = new HashMap<>();

        Bukkit.getPluginManager().registerEvents(new PlayerListeners(), ClipAPI.getInstance());
    }

    public void kickAll(String message) {
        players.forEach(clipPlayer -> clipPlayer.kickPlayer(message));
    }

    public void addPlayer(ClipPlayer clipPlayer) {
        players.add(clipPlayer);
        playerToClipPlayer.put(clipPlayer.getUniqueId(), clipPlayer);

        ClipAPI.getClipDatabase().init(clipPlayer);
    }

    public void removePlayer(ClipPlayer clipPlayer) {
        players.remove(clipPlayer);
        playerToClipPlayer.remove(clipPlayer.getUniqueId());
    }

    public int getSize() {
        return players.size();
    }

    public List<ClipPlayer> getPlayers() {
        return players;
    }

    public ClipPlayer getPlayer(Player player) {
        return playerToClipPlayer.get(player.getUniqueId());
    }

    private final class PlayerListeners implements Listener {
        @EventHandler(priority = EventPriority.HIGHEST)
        public void onPlayerLogin(PlayerLoginEvent event) {
            ClipPlayer clipPlayer = new ClipPlayer(event.getPlayer());

            addPlayer(clipPlayer);

            clipPlayer.getRank().getTeam().addEntry(clipPlayer.getName());
        }

        @EventHandler
        public void onLogin(PlayerJoinEvent event) {
            ClipPlayer clipPlayer = new ClipPlayer(event.getPlayer());

            clipPlayer.setGameMode(GameMode.ADVENTURE);
            clipPlayer.getInventory().clear();
            clipPlayer.getActivePotionEffects().clear();
            clipPlayer.setExp(0);
            clipPlayer.setCollidable(false);

            clipPlayer.setScoreboard(TeamManager.getScoreboard());
        }

        @EventHandler
        public void onPlayerQuit(PlayerQuitEvent event) {
            ClipPlayer clipPlayer = getPlayer(event.getPlayer());

            if (clipPlayer.getIndividualScoreboard() != null)
                clipPlayer.getIndividualScoreboard().end();

            clipPlayer.getRank().getTeam().removeEntry(clipPlayer.getName());

            clipPlayer.getCache().save();
            removePlayer(clipPlayer);
        }
    }
}
