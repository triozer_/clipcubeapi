package fr.clipcube.api.spigot.i18n;

import fr.clipcube.api.spigot.player.ClipPlayer;
import fr.clipcube.api.spigot.player.rank.Rank;

/**
 * @author Triozer
 */
public class SimplyTranslation {

    public static String translateRank(ClipPlayer clipPlayer, Rank rank) {
        if (rank == Rank.ADMINISTRATOR || rank == Rank.DEVELOPER) {
            return clipPlayer.translate("rank." + rank.getName().toLowerCase());
        }

        return rank.getName();
    }

    public static String translateClipName(ClipPlayer clipPlayer, ClipPlayer target) {
        return target.getRank().getTag() + "[" + translateRank(clipPlayer, target.getRank()) + "] " + target.getName();
    }

}
