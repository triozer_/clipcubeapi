package fr.clipcube.api.spigot.i18n;

import fr.clipcube.api.spigot.ClipAPI;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

/**
 * @author Triozer
 */
public enum ClipLanguage {

    FRANCAIS("Français", "fr-FR.yml"),
    ENGLISH("English", "en-US.yml");

    private String            lang;
    private YamlConfiguration file;

    ClipLanguage(String lang, String file) {
        this.lang = lang;
        this.file = YamlConfiguration.loadConfiguration(new File(ClipAPI.getInstance().getDataFolder(), file));
    }

    public String getLang() {
        return lang;
    }

    public YamlConfiguration getFile() {
        return file;
    }

    public static String translate(ClipLanguage clipLanguage, String path, String... replace) {
        if (clipLanguage.getFile() == null)
            return "§cUnable to parse §l" + path + " §cfile is null.";

        if (!clipLanguage.getFile().contains(path))
            return clipLanguage.getFile().getString("path-void").replace("{0}", path);

        String translate = clipLanguage.getFile().getString(path);

        for (int i = 0; i < replace.length; i++)
            translate = translate.replace("{" + i + "}", replace[i]);

        return translate;
    }

    public static ClipLanguage langToEnum(String lang) {
        switch (lang) {
            case "Français":
                return FRANCAIS;
            case "English":
                return ENGLISH;
            default:
                return null;
        }
    }
}
