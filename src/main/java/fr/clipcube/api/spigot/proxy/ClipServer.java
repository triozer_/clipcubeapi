package fr.clipcube.api.spigot.proxy;

import fr.clipcube.api.spigot.ClipAPI;
import fr.clipcube.api.spigot.player.ClipPlayer;
import org.bukkit.craftbukkit.v1_10_R1.CraftServer;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author Triozer
 */
public class ClipServer {

    private String      id;
    private String      name;
    private CraftServer craftServer;

    private List<ClipPlayer> players;

    public ClipServer(CraftServer craftServer) {
        this.name = craftServer.getName();
        this.id = "hub-" + UUID.randomUUID().toString().substring(0, 4);
        this.craftServer = craftServer;

        players = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public CraftServer getCraftServer() {
        return craftServer;
    }

    public List<ClipPlayer> getPlayers() {
        players.clear();

        craftServer.getOnlinePlayers()
                .forEach(craftPlayer -> players.add(ClipAPI.getPlayerManager().getPlayer(craftPlayer)));

        return players;
    }
}
