package fr.clipcube.api.spigot.proxy;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

/**
 * @author Triozer
 */
public class ProxyListener implements PluginMessageListener {

    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] message) {
        if (!channel.equals("BungeeCord")) {
            return;
        }

        if (channel.equals("ClipAPI")) {
            Bukkit.broadcastMessage("MESSAGE FROM BUNGEECORD");
        }
    }

}