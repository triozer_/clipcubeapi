package fr.clipcube.api.spigot.util;

import fr.clipcube.api.spigot.ClipAPI;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Triozer
 */
public class HologramBuilder {

    private List<ArmorStand> armorStands;
    private String[]         lines;
    private Location         location;
    private Item             item;
    private int time = -1;

    public HologramBuilder(Location location) {
        armorStands = new ArrayList<>();
        this.location = location;
        Bukkit.getPluginManager().registerEvents(new HologramListener(), ClipAPI.getInstance());
    }

    public HologramBuilder lines(String... lines) {
        this.lines = new String[lines.length];

        for (int i = 0; i < lines.length; i++)
            this.lines[i] = lines[i];

        return this;
    }

    public HologramBuilder time(int time) {
        this.time = time;

        return this;
    }

    public HologramBuilder item(ItemStack itemStack) {
        this.item = location.getWorld().dropItem(getLocation(), itemStack);
        item.setPickupDelay(Integer.MAX_VALUE);

        return this;
    }

    public HologramBuilder build() {
        if (item == null)
            for (int i = 0; i < lines.length; i++) {
                ArmorStand armorStand = (ArmorStand) location.getWorld().spawnEntity(location.subtract(0, 0.25, 0), EntityType.ARMOR_STAND);

                armorStand.setVisible(false);
                armorStand.setGravity(false);

                armorStand.setCustomNameVisible(false);

                if (!lines[i].equals("")) {
                    armorStand.setCustomName(lines[i]);
                    armorStand.setCustomNameVisible(true);
                }

                armorStands.add(armorStand);
            }
        else {
            ArmorStand armorStand = (ArmorStand) location.getWorld().spawnEntity(location.subtract(0, 0.25, 0), EntityType.ARMOR_STAND);

            armorStand.setVisible(false);
            armorStand.setGravity(false);

            armorStand.setPassenger(item);

            armorStands.add(armorStand);
        }

        if (time != -1)
            Bukkit.getScheduler().runTaskLater(ClipAPI.getInstance(), () -> end(), 20 * time);

        return this;
    }

    public void end() {
        armorStands.forEach(armorStand -> armorStand.remove());

        if (item != null)
            item.remove();
    }

    public void updateLines(String... lines) {
        for (int i = 0; i < lines.length; i++) {
            ArmorStand armorStand = armorStands.get(i);

            armorStand.setCustomNameVisible(false);

            if (!lines[i].equals("")) {
                armorStand.setCustomName(lines[i]);
                armorStand.setCustomNameVisible(true);
            }
        }
    }

    public List<ArmorStand> getArmorStands() {
        return armorStands;
    }

    public Location getLocation() {
        return location;
    }

    public String[] getLines() {
        return lines;
    }

    public Item getItem() {
        return item;
    }

    private final class HologramListener implements Listener {
        @EventHandler
        public void onInteract(PlayerInteractAtEntityEvent event) {
            if (event.getRightClicked() instanceof ArmorStand)
                armorStands.stream().filter(armorStand -> event.getRightClicked() == armorStand)
                        .forEach(armorStand -> {
                            event.setCancelled(true);
                        });
        }
    }
}
