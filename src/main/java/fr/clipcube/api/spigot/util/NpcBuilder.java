package fr.clipcube.api.spigot.util;

import fr.clipcube.api.spigot.ClipAPI;
import fr.clipcube.api.spigot.player.ClipPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;

import java.util.UUID;

/**
 * @author Triozer
 */
public class NpcBuilder {

    private String   id;
    private String[] lines;

    private Villager            villager;
    private Villager.Profession profession;

    private Location location;

    private HologramBuilder hologramBuilder;

    private NpcListener listener;

    public NpcBuilder(Location location) {
        this.id = UUID.randomUUID().toString().substring(4);
        this.location = location;

        Bukkit.getPluginManager().registerEvents(new NpcListeners(), ClipAPI.getInstance());
    }

    public NpcBuilder name(String... lines) {
        this.lines = new String[lines.length];
        for (int i = 0; i < lines.length; i++) this.lines[i] = lines[i];

        return this;
    }

    public NpcBuilder name(HologramBuilder hologramBuilder) {
        this.hologramBuilder = hologramBuilder;

        return this;
    }

    public NpcBuilder profession(Villager.Profession profession) {
        this.profession = profession;

        return this;
    }

    public NpcBuilder addListener(NpcListener listener) {
        this.listener = listener;

        return this;
    }

    public NpcBuilder build() {
        villager = (Villager) location.getWorld().spawnEntity(location, EntityType.VILLAGER);

        villager.setAI(false);
        villager.setGravity(false);
        villager.setSilent(true);
        villager.setCollidable(false);

        if (profession == null)
            villager.setProfession(Villager.Profession.LIBRARIAN);
        else
            villager.setProfession(profession);

        if (hologramBuilder == null)
            hologramBuilder = new HologramBuilder(villager.getLocation().add(0, lines.length * 0.25, 0)).lines(lines).build();

        return this;
    }

    public void end() {
        hologramBuilder.end();

        if (!villager.isDead())
            villager.remove();
    }

    public String getId() {
        return id;
    }

    public Villager getVillager() {
        return villager;
    }

    public String[] getLines() {
        return lines;
    }

    public Location getLocation() {
        return location;
    }

    public static class NpcListener {
        public void onClick(PlayerInteractEntityEvent event, ClipPlayer clipPlayer) {
            event.setCancelled(true);
        }
    }

    private final class NpcListeners implements Listener {
        @EventHandler
        public void onDeath(EntityDeathEvent event) {
            if (event.getEntity() instanceof Villager && event.getEntity() == villager) {
                end();
            }
        }

        @EventHandler
        public void onHit(EntityDamageByEntityEvent event) {
            if (event.getEntity() instanceof Villager && event.getEntity() == villager) {
                event.setCancelled(true);
            }
        }

        @EventHandler
        public void onClick(PlayerInteractEntityEvent event) {
            if (event.getRightClicked() instanceof Villager && event.getRightClicked() == villager) {
                if (listener != null)
                    listener.onClick(event, ClipAPI.getPlayerManager().getPlayer(event.getPlayer()));
                else
                    event.setCancelled(true);
            }
        }
    }
}