package fr.clipcube.api.spigot.util;

/**
 * @author Triozer
 */
public class Text {

    public final static String SCOREBOARD_HEADER = "§b§lClip§6§o§lCube §b§lNetwork";
    public final static String SCOREBOARD_FOOTER = "- §b§lT&K | §oDevelopement §r-";
    public final static String TABLIST_HEADER    = "§b§lClip§6§o§lCube §b§lNetwork";
    public final static String TABLIST_FOOTER    = "§bTwitter: §o@clipcube";
    public final static String DEFAULT_MOTD      = "§b§lClip§6§o§lCube §b§l- Youtube tutorial !";

}