package fr.clipcube.api.spigot.util;

import fr.clipcube.api.spigot.ClipAPI;
import fr.clipcube.api.spigot.player.ClipPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

/**
 * @author Triozer
 */
public class InventoryBuilder {

    private String    name;
    private int       size;
    private Inventory inventory;

    private InventoryListener listener;

    public InventoryBuilder(String name, int size) {
        this.name = name;
        this.size = size;

        inventory = Bukkit.createInventory(null, size, name);
    }

    public InventoryBuilder(String name, InventoryType inventoryType) {
        this.name = name;
        this.size = inventoryType.getDefaultSize();

        inventory = Bukkit.createInventory(null, inventoryType, name);
    }

    public InventoryBuilder addItem(ItemStack... itemStacks) {
        inventory.addItem(itemStacks);

        return this;
    }

    public InventoryBuilder setItem(int slot, ItemStack itemStack) {
        inventory.setItem(slot, itemStack);

        return this;
    }

    public InventoryBuilder fill(ItemStack itemStack) {
        for (int i = 0; i < size; i++)
            setItem(i, itemStack);

        return this;
    }

    public InventoryBuilder fillLine(ItemStack itemStack, int line) {
        for (int i = line; i < line + 9; i++)
            setItem(i, itemStack);

        return this;
    }

    public InventoryBuilder setListener(InventoryListener listener) {
        this.listener = listener;

        return this;
    }

    public void removeItem(int slot) {
        inventory.clear(slot);
    }

    public void removeItem(ItemStack itemStack) {
        inventory.remove(itemStack);
    }

    public void clear() {
        inventory.clear();
    }

    public Inventory build(Plugin plugin) {
        Bukkit.getPluginManager().registerEvents(new InnerInventoryListener(), plugin);

        return inventory;
    }

    public String getName() {
        return name;
    }

    public int getSize() {
        return size;
    }

    public static abstract class InventoryListener {
        public abstract void interact(ClipPlayer player, InventoryClickEvent event);
    }

    // TODO: A LISTENER IN A LISTENER DOESNT WORK.
    public final class InnerInventoryListener implements Listener {
        @EventHandler
        public void interact(InventoryClickEvent event) {
            if (event.getCurrentItem() == null || event.getCurrentItem().getType() == Material.AIR)
                return;

            if (listener != null && event.getInventory() == inventory) {
                listener.interact(ClipAPI.getPlayerManager().getPlayer((Player) event.getWhoClicked()), event);
            }
        }
    }
}
