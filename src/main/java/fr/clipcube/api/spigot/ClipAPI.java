package fr.clipcube.api.spigot;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import fr.clipcube.api.database.ClipDatabase;
import fr.clipcube.api.spigot.command.*;
import fr.clipcube.api.spigot.ishield.IShield;
import fr.clipcube.api.spigot.listener.GuiListener;
import fr.clipcube.api.spigot.listener.PlayerListener;
import fr.clipcube.api.spigot.player.PlayerManager;
import fr.clipcube.api.spigot.proxy.ClipServer;
import fr.clipcube.api.spigot.scoreboard.TeamManager;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Triozer
 */
public class ClipAPI extends JavaPlugin {
    private static List<ClipModule> clipModule = new ArrayList<>();
    private static ClipAPI         instance;
    private static ClipDatabase    clipDatabase;
    private static ClipServer      clipServer;
    private static PlayerManager   playerManager;
    private        ProtocolManager protocolManager;

    public static ClipAPI getInstance() {
        return instance;
    }

    public static PlayerManager getPlayerManager() {
        return playerManager;
    }

    public static ClipDatabase getClipDatabase() {
        return clipDatabase;
    }

    public ClipServer getClipServer() {
        return clipServer;
    }

    public ProtocolManager getProtocolManager() {
        return protocolManager;
    }

    public static List<ClipModule> getClipModule() {
        return clipModule;
    }

    @Override
    public void onLoad() {
        registerModules();

        protocolManager = ProtocolLibrary.getProtocolManager();
        instance = this;

        clipModule.stream().forEach(module -> module.onLoad());
    }

    @Override
    public void onEnable() {
        Bukkit.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");

        if (!getDataFolder().exists() || Arrays.asList(getDataFolder().listFiles()).isEmpty()) {
            saveResource("fr-FR.yml", false);
            saveResource("en-US.yml", false);
        }

        clipModule.stream().forEach(module -> module.onEnable());

        playerManager = new PlayerManager();

        clipDatabase = new ClipDatabase("127.0.0.1", 3307, "clip", "root", "");
        // clipDatabase = new ClipDatabase("127.0.0.1", 3306, "clipcube", "root", "triozer97680");

        TeamManager.init();

        clear();

        registerCommands();
        registerListeners();
    }

    @Override
    public void onDisable() {
        clipModule.stream().forEach(module -> module.onDisable());

        if (!playerManager.getPlayers().isEmpty()) {
            playerManager.getPlayers().stream().forEach(clipPlayer -> clipPlayer.getCache().save());
            playerManager.kickAll("Server is reloading.");
        }
    }

    private void registerCommands() {
        new AlertCommand().register(this);
        new CoinsCommand().register(this);
        new ClipFreeze().register(this);
        new InfoCommand().register(this);
        new LanguageCommand().register(this);
        new RankCommand().register(this);
    }

    private void registerListeners() {
        Bukkit.getPluginManager().registerEvents(new GuiListener(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerListener(), this);
    }

    private void registerModules() {
        new IShield();
    }

    private void clear() {
        for (World worlds : Bukkit.getWorlds())
            worlds.getEntities().forEach(Entity::remove);
    }

    public void initClipServer(ClipServer clipServer) {
        this.clipServer = clipServer;
    }

}